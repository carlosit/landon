### Landon

Web site for reservations rooms deployed with [Laravel](https://laravel.com) 5.7 and [Foundation](https://foundation.zurb.com/) CSS
originally from tutorial [Bernard Pineda](https://www.lynda.com/Bernardo-Pineda/6090273-1.html)

<p align="left">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>